//=============================================================================
// AltMenuScreen.js
//=============================================================================

/*:
 * @plugindesc Custom Alt Menu
 * @author Frosty
 *
 * @param BackgroundPicture
 * @desc The menu background - add picture to img/pictures/ (i.e "Sword")
 * @default
 * 
 * @param MenuOpacity
 * @desc The menu opacity (0 - 256 (?-max value))
 * @default 50
 *
 * @help
  * ============================================================================
 * Introduction
 * ============================================================================
 *
 * This plugin adds the ability to adjust the background image and opacity of 
 * critical menu screens.
 *
 */

(function() {

    var parameters = PluginManager.parameters("CustomAltMenu");
    var background = String(parameters["BackgroundPicture"]);
    var customOpacity = Number(parameters["MenuOpacity"]);

    
    var _Scene_MenuBase_createBackground = Scene_MenuBase.prototype.createBackground; 
    Scene_MenuBase.prototype.createBackground = function() {
        if(background){
            this._backgroundSprite = new Sprite();
            this._backgroundSprite.bitmap = ImageManager.loadPicture(background);
            this.addChild(this._backgroundSprite);
        } else {
            _Scene_MenuBase_createBackground.call(this)
        }
    }

    var _Scene_Menu_create = Scene_Menu.prototype.create;
    Scene_Menu.prototype.create = function() {
        _Scene_Menu_create.call(this);
        
        this._commandWindow.x = 0; //this._statusWindow.width;
        this._commandWindow.y = 0;

        this._statusWindow.x = 0;
        this._statusWindow.y = this._commandWindow.height;

        this._goldWindow.x = this._statusWindow.width;
        this._goldWindow.y = this._commandWindow.height;

        this._commandWindow.opacity = customOpacity;
        this._statusWindow.opacity = customOpacity;
        this._goldWindow.opacity = customOpacity;

    };

    var _Scene_Item_create = Scene_Item.prototype.create;
    Scene_Item.prototype.create = function() {
        _Scene_Item_create.call(this);

        this._categoryWindow.opacity = customOpacity;
        this._itemWindow.opacity = customOpacity;
        this._helpWindow.opacity = customOpacity;
        this._actorWindow.opacity = customOpacity;
    };

    var _Scene_Equip_create = Scene_Equip.prototype.create;
    Scene_Equip.prototype.create = function() {
        _Scene_Equip_create.call(this);

        this._helpWindow.opacity = customOpacity;
        this._statusWindow.opacity = customOpacity;
        this._commandWindow.opacity = customOpacity;
        this._slotWindow.opacity = customOpacity;
        this._itemWindow.opacity = customOpacity;
    };

    var _Scene_Skill_create = Scene_Skill.prototype.create;
    Scene_Skill.prototype.create = function() {
        _Scene_Skill_create.call(this);

        this._helpWindow.opacity = customOpacity;
        this._skillTypeWindow.opacity = customOpacity;
        this._itemWindow.opacity = customOpacity;
        this._statusWindow.opacity = customOpacity;
        this._actorWindow.opacity = customOpacity;
    };

    var _Scene_Status_create = Scene_Status.prototype.create;
    Scene_Status.prototype.create = function() {
        _Scene_Status_create.call(this);

        this._statusWindow.opacity = customOpacity;
    };

    var _Scene_Class_create = Scene_Class.prototype.create;
    Scene_Class.prototype.create = function() {
        _Scene_Class_create.call(this);

        this._helpWindow.opacity = customOpacity;
        this._commandWindow.opacity = customOpacity;
        this._helpWindow.opacity = customOpacity;
        this._statusWindow.opacity = customOpacity;
        this._itemWindow.opacity = customOpacity;
        this._compareWindow.opacity = customOpacity;
    };

    /*Menu Status Window Manipulation*/

    Window_MenuStatus.prototype.windowWidth = function() {
        return Graphics.boxWidth - 240;
    };

    Window_MenuStatus.prototype.windowHeight = function() {
        return Graphics.boxHeight / 2;
    };

    Window_MenuStatus.prototype.numVisibleRows = function() {
        return 2;
    };

    Window_MenuStatus.prototype.maxCols = function() {
        return 1;
    };

    /*Menu Command Window Manipulation*/

    Window_MenuCommand.prototype.windowWidth = function() {
        return Graphics.boxWidth;
    };

    Window_MenuCommand.prototype.windowHeight = function() {
        return this.fittingHeight(this.numVisibleRows());
    };

    Window_MenuCommand.prototype.numVisibleRows = function() {
        return 3;
    };

    Window_MenuCommand.prototype.maxCols = function() {
        return 4;
    };

    /*Menu Gold Window Manipulation*/

    //Empty



})();
